package com.example.ventas.appcreacionusuario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class UsuarioActivity extends AppCompatActivity {
    private EditText txtnombre,txtapellido,txtpassword,txtvalidarpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        txtnombre= (EditText) findViewById(R.id.txtnombre);
        txtapellido= (EditText) findViewById(R.id.txtapellido);
        txtpassword= (EditText) findViewById(R.id.txtpassword);
        txtvalidarpassword= (EditText) findViewById(R.id.txtvalidarpassword);
    }
    
    public void ingresar(View view){
        String nom=txtnombre.getText().toString();
        String ape=txtapellido.getText().toString();
        String pas=txtpassword.getText().toString();
        String valpas=txtvalidarpassword.getText().toString();
        if (pas.equals(valpas)){
            Intent i=new Intent(this, InteresesActivity.class);
            startActivity(i);
        }else {
            Toast toast =Toast.makeText(this,"no coinciden las contraseña",Toast.LENGTH_SHORT);
            toast.show();

        }
    }
}
